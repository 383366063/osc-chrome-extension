function getLatestNewsRss() {
    return "https://www.oschina.net/news/rss";
}

function getLatestQuestionsRss() {
    return "https://www.oschina.net/question/rss";
}

function getRecommendBlogsRss() {
    return "https://www.oschina.net/blog/rss?show=more";
}

function getRecommandProjectsRss() {
    return "https://www.oschina.net/project/rss?show=recomms";
}

function getUtmSourcePatten() {
    return "utm_source=oschina-chrome-extension";
}