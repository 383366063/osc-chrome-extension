# osc-chrome-extension

#### 概要
开源中国 (oschina.net) 谷歌浏览器插件，聚合开源中国每日最新资讯、推荐精品博客、最新收录开源软件等优质内容。

![latest-snapshot](snapshots/snapshot_v1.3.jpg)

#### 安装使用方式：通过 [Chrome Web Store 下载](https://chrome.google.com/webstore/detail/%E5%BC%80%E6%BA%90%E4%B8%AD%E5%9B%BD/ppagmdehdibgipjkjbijngfphoboggdf) (注意打开方式)

#### 安装使用方式：通过源码安装
1. git clone 本项目
2. 打开 Chrome 浏览器 Extensions 页面
3. 打开「Developer mode」
4. 选择左上角「Load unpacked」之后选择本仓库的根目录

#### 如何参与本项目贡献
如果你觉得这项目挺有意思，亦或想要吐槽，欢迎通过以下方式提交 PR

1.  fork 本仓库
2.  新建 feature_xxx 分支或 bugfix_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 最后，欢迎吐槽！ Enjoy. 
